﻿export default [
  {
    path: '/',
    component: '../layouts/BlankLayout',
    routes: [
      {
        path: '/user',
        component: '../layouts/UserLayout',
        routes: [
          {
            name: 'login',
            path: '/user/login', // url路径 可以不变
            component: './Login/index',
          },
        ],
      },
      {
        path: '/',
        component: '../layouts/SecurityLayout',
        routes: [
          {
            path: '/',
            component: '../layouts/BasicLayout',
            authority: ['admin', 'user'],
            routes: [
              {
                path: '/',
                redirect: '/welcome',
              },
              {
                path: '/welcome',
                name: '欢迎',
                icon: 'smile',
                component: './Welcome',
              },
              {
                path: '/admin',
                name: '管理员',
                icon: 'crown',
                component: './Admin',
                authority: ['admin'],
                routes: [
                  {
                    path: '/admin/sub-page',
                    name: '子页面',
                    icon: 'smile',
                    component: './Welcome',
                    authority: ['admin'],
                  },
                ],
              },
              {
                name: '表格',
                icon: 'table',
                path: '/list',
                component: './TableList',
              },
              // 我们在这里添加 helloworld 的路由配置
              {
                path: '/hello-world', // 路由路径 和vue一致
                 // name并非路由"别名", 这个和vue是不同的. 
                 // 这里的name体现了sideBar的导航条目,由框架代码实现.
                name: '世界俺来了',  
                 // # https://ant.design/components/icon-cn/
                icon: 'smile',
                 // 组建相对路径为 /pages
                component: './HelloWorld'
               },
               {
                path: '/content',
                name: '内容管理',
                icon: 'crown',
                routes: [
                  {
                    path: '/content/article-list',
                    name: '文章管理',
                    icon: 'smile',
                    component: './content/Article',
                  },
                ],
              },
              {
                component: './404',
              },
            ],
          },
          {
            component: './404',
          },
        ],
      },
    ],
  },
  {
    component: './404',
  },
];
