import { LogoutOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Menu, Spin } from 'antd';
import React from 'react';
import type { ConnectProps } from 'umi';
import { history, connect } from 'umi';
// import type { ConnectState } from '@/models/connect';
// import type { CurrentUser } from '@/models/user';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import {getUserInfo} from '@/utils/myAuth';

export type GlobalHeaderRightProps = {
  menu?: boolean;
} & Partial<ConnectProps>;

class AvatarDropdown extends React.Component<GlobalHeaderRightProps> {
  // 事件委托形式实现退出按钮的点击
   onMenuClick = (event: {
    key: React.Key;
    keyPath: React.Key[];
    item: React.ReactInstance;
    domEvent: React.MouseEvent<HTMLElement> | React.KeyboardEvent<HTMLElement>;
  }) => {
    const { key } = event;

    if (key === 'logout') {
      const { dispatch } = this.props;
      // 把退出逻辑交给 myLogin的model处理
      if (dispatch) {
        dispatch({
          type: 'myLogin/doLogout',
        });
      }
      return;
    }

    history.push(`/account/${key}`);
  };
  // 在class中单独声明一个变量 类似于直接声明到了构造函数里
  userInfo = getUserInfo();
  render(): React.ReactNode {
    const {
      menu,
    } = this.props;
    const menuHeaderDropdown = (
      <Menu className={styles.menu} selectedKeys={[]} onClick={this.onMenuClick}>
        {menu && (
          <Menu.Item key="center">
            <UserOutlined />
            个人中心
          </Menu.Item>
        )}
        {menu && (
          <Menu.Item key="settings">
            <SettingOutlined />
            个人设置
          </Menu.Item>
        )}
        {menu && <Menu.Divider />}

        <Menu.Item key="logout">
          <LogoutOutlined />
          退出登录
        </Menu.Item>
      </Menu>
    );
    return this.userInfo && this.userInfo.nickname ? (
      <HeaderDropdown overlay={menuHeaderDropdown}>
        <span className={`${styles.action} ${styles.account}`}>
          {/* // this.userInfo? 是一种安全机制 如果存在再获取 */}
          <Avatar size="small" className={styles.avatar} src={this.userInfo?.icon} alt="avatar" />
          <span className={`${styles.name} anticon`}>{this.userInfo.nickname}</span>
        </span>
      </HeaderDropdown>
    ) : (
      <span className={`${styles.action} ${styles.account}`}>
        <Spin
          size="small"
          style={{
            marginLeft: 8,
            marginRight: 8,
          }}
        />
      </span>
    );
  }
}

export default connect(() => ({
  
}))(AvatarDropdown);
