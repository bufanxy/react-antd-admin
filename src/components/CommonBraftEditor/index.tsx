import React, { useEffect, useState } from 'react';
import BraftEditor from 'braft-editor';
import type { EditorState } from 'braft-editor';
import 'braft-editor/dist/index.css';
import { message } from 'antd';
// # https://www.yuque.com/braft-editor/be/mrgy92
// import { ContentUtils } from 'braft-utils'; 不支持es6模块话,尝试用commonjs引入
const { ContentUtils } = require('braft-utils');

// 如果单独作为一个组件,应该接收哪写props?
type PropsType = {
  value: string; // 用于初始化富文本内容
  uploadUrl: string ;  // 上传的url
  onChange: (content: string) => any; // 用于处理变化的回调
  token: string ;  // token
}
const CommonBraftEditor: React.FC<PropsType> = (props)=>{
  const {value,uploadUrl,token,onChange} = props;
  // 定义editorState
  const [editorState,setEditorState]  = useState<EditorState>(null);
  // 受控组件 根据内容变化更新editorState
  const handleEditorChange = (state: EditorState)=>{
    setEditorState(state);
    // 把最新的html回调给外部
    onChange(state.toHTML());
  }
  const submitContent = ()=>{
    onChange(editorState.toHTML());
  }
  // 自定义上传
  const myUploadFn = (param: any) => {

    const serverURL = uploadUrl;
    const xhr = new XMLHttpRequest;
    const fd = new FormData();
    // 这个demo代码回报错 用自己实现的方法
    // const successFn = (response) => {
    //   // 假设服务端直接返回文件上传后的地址
    //   // 上传成功后调用param.success并传入上传后的文件地址
    //   param.success({
    //     url: xhr.responseText,
    //     meta: {
    //       id: 'xxx',
    //       title: 'xxx',
    //       alt: 'xxx',
    //       loop: true, // 指定音视频是否循环播放
    //       autoPlay: true, // 指定音视频是否自动播放
    //       controls: true, // 指定音视频是否显示控制栏
    //       poster: 'http://xxx/xx.png', // 指定视频播放器的封面
    //     }
    //   })
    // }
  
    const progressFn = (event: any) => {
      // 上传进度发生变化时调用param.progress
      param.progress(event.loaded / event.total * 100)
    }
  
    const errorFn = () => {
      // 上传发生错误时调用param.error
      param.error({
        msg: 'unable to upload.'
      })
    }
  
    xhr.upload.addEventListener("progress", progressFn, false);
    // xhr.addEventListener("load", successFn, false);
    xhr.addEventListener("error", errorFn, false);
    xhr.addEventListener("abort", errorFn, false);
  
    fd.append('file', param.file);
    xhr.open('POST', serverURL, true);
    // 再open之后添加token
    xhr.setRequestHeader('token',token);
    xhr.send(fd);

    // 监听请求进度
    xhr.onreadystatechange = ()=>{
      // readyState == 4 说明请求过程已完成
      // status == 200 说明http请求内容成功，数据可用
      if(xhr.readyState === 4 && xhr.status === 200){
          // console.log(xhr.responseText);
        const response = JSON.parse(xhr.responseText);
        const {success,data} = response;
        if(success){
          const {fileUrl} = data;
          // 把得到的回调url回填到富文本
          param.success({
            url: fileUrl,
              meta: {
                id: new Date().getTime()
              }
            })
        }else{
          message.error('上传失败!');
        }
      }
    }
  
  }
  // 箭头value的变化,对editorState进行初始化
  useEffect(()=>{
    if(value){
      setEditorState(BraftEditor.createEditorState(value));
    }else{
      setEditorState(BraftEditor.createEditorState(''));
    }
  },[value]);
  

  return (
    <BraftEditor
        style={{ border: '1px solid #e5e5e5' }}
        value={editorState}
        onChange={handleEditorChange}
        onSave={submitContent}
        media={{uploadFn: myUploadFn}}
      />
  )
}

export default CommonBraftEditor;