import React from 'react';
import {Form,Input,Button} from 'antd';
import styles from './index.less';
// umi对dva进行了集成,所以connect从umi获取
import { connect } from 'umi';
// 按照直接的思维方式考虑umi肯定定义过了Dispatch类型,所以尝试从umi导入该类型
import type {Dispatch } from 'umi';
import type {TUserInfo,MStateType} from './model';
import type { TLoginParams} from '@/services/login';
import type {ConnectState} from '@/models/connect.d';
// 全局声明 命名空间
const namespace = 'myLogin';
// 定义props类型
type TProps  = {
  dispatch: Dispatch;
  userInfo: TUserInfo|{};
}
// 函数式组件没有this,在class中通过this.props获取props,在函数中应该通过形参直接获取
// 只要使用了React.FC 只要里面使用props,就需要以泛型形式引入props类型
const Login: React.FC<TProps> = (props)=>{
  // 需要通过props获取dispatch ? 能获取吗?
  // var dispatch = props.dispatch;
  // ts的错误暂时不处理,随后会专门讲ts
  const {dispatch,userInfo} = props;
  // 注意:箭头函数形式 函数式组件,没有this
  const onFinish = (values: TLoginParams) => {
    // console.log('Success:', values);
    // 希望dispatch 一个 effect把username,password传递过去
    // dispatch is not a function????
    // dispatch是从props获取的没错,但必须把model和当前组件进行关联,才能获取dispach对象
    dispatch({
      type: `${namespace}/doLogin`,
      payload: {
        ...values
      }
    })
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <Form
      className = {styles.main}
      labelCol = {
        {span: 8}
      }
      wrapperCol = {
        {span: 16}
      }
      name="login"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="用户名"
        name="username"
        rules={[{ required: true, message: '请输入用户名!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="密码"
        name="password"
        rules={[{ required: true, message: '请输入密码!' }]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item wrapperCol={
        {
          offset: 8,
          span: 16
        }
      }>
        <Button type="primary" htmlType="submit">
          提交
        </Button>
      </Form.Item>
      {JSON.stringify(userInfo)}
    </Form>
  )
}

// connect 是用于连接model和component的,要不然在组件里无法获取model的状态
// 文档: # https://dvajs.com/guide/introduce-class.html#connect-%E6%96%B9%E6%B3%95
// state 从哪来的? 是不是就是Model中的state?
// 以后关于connect的state不要单独定义了,可以从moudels/connect.d.ts中引入再合并
type StateType = {
  [namespace]: MStateType;
}&ConnectState;
const mapStateToProps = (state: StateType)=>{
  // console.log('state',state);
  return {
    userInfo: state[namespace].userInfo
  }
}
// 把model和组件关联起来
export default connect(mapStateToProps)(Login);