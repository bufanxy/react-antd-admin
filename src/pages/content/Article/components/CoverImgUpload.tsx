import React, { useEffect, useState } from 'react';
// 直接引入antd内部接口类型
import type { UploadFile } from 'antd/lib/upload/interface';
import { UploadOutlined } from '@ant-design/icons';
import {Button, message, Upload } from 'antd';

// 组件封装的两个原则: 1. 考虑代码/功能的复用性; 2. 考虑代码的可读性
// 重新定义给当前组件使用的局部组件 CoverImgUpload
// 定义propType
type CoverImgPropsType = {
  action: string;
  token: string;
  coverImg: string; // 用于组件的回显 
  onChange: (fileUrl: string) => any; // 组件希望和外部通信,可以通过onChange回调把需要处理的数据传递出去
}
// 当前组件通信的需求: 
//  1. 回显的时候,需要外部传入imgUrl
//  2. upload上传成功 需要通知外部更改外部的某些状态
const CoverImgUpload: React.FC<CoverImgPropsType> = (props)=>{
  const [fileList,setFileList] = useState<UploadFile[]>([]);
  const {action,token,coverImg,onChange} = props;
  // coverImg是从外部传入进来的,想要再上传组件显示,必须构建一个UploadFile对象并放入fileList列表
  useEffect(()=>{
    if(coverImg){
      // 处理upload组件 既然是受控组件,只需要修改 fileList即可
      // 返回的数据只有url  所以需要构建 UploadFile数据类型对象
      // https://bufan-apitown.oss-cn-beijing.aliyuncs.com/bftec/20210610111012786peiqi.jpg
      // ===>  20210610111012786peiqi.jpg
      const url: string = coverImg;
      const fileName: string = url.substr(url.lastIndexOf('/') + 1, url.length - 1);
      const file: UploadFile = { uid: new Date().getTime().toString(), name: fileName, url: coverImg };
      // 这里只是回显了图片 ,但是需要注意 表单的字段 coverImg也需要设置
      setFileList([file]);
    }else{
      setFileList([]);
    }
  },[coverImg])

  return (
    <Upload
      name="file"
      action={action}
      listType="picture"
      maxCount={1}
      headers={{
        token
      }}
      fileList={fileList}
      onChange={({ file, fileList: fList }) => {
        if (file.status === 'uploading') {
          // console.log('uploading', file, fList);
        }
        // 如果完成了,肯定希望有回调结果(服务器图片的真实路径)
        if (file.status === 'done') {
          message.success(`${file.name} file uploaded successfully`);
          // 得到回调结果
          const { response } = file;
          // 判断成功失败
          const { success, message: errMsg, data } = response;
          if (success) {
            // 成功后需要把服务器回调的图片地址存储起来
            const { fileUrl } = data;
            // 组件内部得到fileUrl 需要传递到外部进行处理
            onChange(fileUrl);
          } else {
            message.error(errMsg);
          }
          // console.log('done..',info.file);
        } else if (file.status === 'error') {
          message.error(`${file.name} file upload failed.`);
        }
        // 重新设置fileList  用于检测更新
        // # https://github.com/ant-design/ant-design/issues/2423
        setFileList(fList);
      }}
      onRemove={() => {
        onChange('');
      }}
    >
      <Button icon={<UploadOutlined />}>点击上传1</Button>
    </Upload>
  )
}

export default CoverImgUpload;