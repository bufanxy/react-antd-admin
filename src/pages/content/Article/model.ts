import {findArticleList as articleListApi} from '@/services/article';
// 类型是从umi取得
import type {Reducer,Effect} from 'umi';
import {message } from 'antd';
// 定义一个article对象类型 方便使用
export type ArticleType = {
  id?: string;
  author: string;
  title: string;
  collectCount?: number;  // 阅读量
  content1?: string;
  content2?: string;
  coverImg?: string;
  createTime?: string;   // 创建时间
  editorType?: number;
  isShow?: number;
  modifyTime?: string;  // 更新时间
  summary?: string;
  viewCount?: number;
  zanCount?: number;
}

export type MStateType = {
  articleList: ArticleType[];  // 需要把结果声明出来,用于在组件显示
  totalCount: number;
}
type MType = {
  namespace: string;
  state: MStateType;
  effects: {
    findArticleList: Effect;
  };
  reducers: {
    setArticleList: Reducer;
  };
}
const M: MType = {
  namespace: 'article',
  state: {
    articleList: [],
    totalCount: 0,
  },
  effects: {
    *findArticleList({payload}, {call,put}){
      // 1. 发送请求  
      const {success,message:errMsg,data} = yield call(articleListApi,payload);
      // 2. 对结果进行处理,保存到state中
      if(success){
        const {rows,total} = data;
        yield put({
          type: 'setArticleList',
          payload: {
            rows,
            total
          }
        })
      }else{
        message.error(errMsg);
      }
    }
  },
  reducers: {
    // 对文章列表进行初始化
    setArticleList(state, {payload}){
      const {rows,total} = payload;
      return {
        ...state,
        articleList: rows,
        totalCount: total
      }
    }
  }
}

export default M;