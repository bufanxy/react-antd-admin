import React from 'react';
import { PageContainer } from '@ant-design/pro-layout';
// 创建组件? 什么类型? React.component类型
// ts有强制类型约束 React.FC react的function component 类型
const HelloWorld: React.FC = ()=>{
  return (
    <PageContainer>
      <div>
        hello,world!
      </div>
    </PageContainer>
  )
}

export default HelloWorld;