import type {TUserInfo} from '@/pages/Login/model';

const TOKEN_KEY = 'REACT_DEMO_TOKEN';
const USER_KEY = 'REACT_USERINFO';

// 定义token的存取
export function setToken(token: string){
  window.localStorage.setItem(TOKEN_KEY,token);
}
export function getToken(){
  const token = window.localStorage.getItem(TOKEN_KEY);
  if(token == null || token === 'undefined'){
    return null;
  }
  return token as string;
}
export function removeToken(){
  window.localStorage.removeItem(TOKEN_KEY);
}

// 定义userInfo的存取
export function saveUserInfo(userInfo: TUserInfo){
  const userInfoStr: string = JSON.stringify(userInfo);
  window.localStorage.setItem(USER_KEY,userInfoStr);
}
export function getUserInfo(){
  const userInfo = window.localStorage.getItem(USER_KEY);
  if(userInfo == null || userInfo === 'undefined'){
    return null
  }
  const userInfoObj: TUserInfo = JSON.parse(userInfo);
  return userInfoObj;
}
export function removeUserInfo(){
  window.localStorage.removeItem(USER_KEY);
}

// 定义移除用户缓存
export function clearUserCache(){
  removeToken();
  removeUserInfo();
}