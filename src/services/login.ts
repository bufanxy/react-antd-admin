import request from '@/utils/request';

export type LoginParamsType = {
  userName: string;
  password: string;
  mobile: string;
  captcha: string;
};
// 定义一个doLogin接口的参数类型
// 建议定义的类型 加上export , 方便在其他任意文件单独引入该类型
export interface ILogInParams{
  username: string;
  password: string;
}
// 可以定义成interface , 也可以定义成type
// 如果不考虑扩展性(比如extends,implement),可以直接使用type
export type TLoginParams = {
  username: string;
  password: string;
}
export async function fakeAccountLogin(params: LoginParamsType) {
  return request('/api/login/account', {
    method: 'POST',
    data: params,
  });
}

export async function getFakeCaptcha(mobile: string) {
  return request(`/api/login/captcha?mobile=${mobile}`);
}

/**
 * 比葫芦画瓢 利用封装的request 实现登陆接口封装
 * @param  {username:xxx,password:xxx}
 * @returns 
 */
export async function doLogin(params: TLoginParams) {
  // fetch
  return request(`/lejuAdmin/index/login`,{
    method: 'POST', //
    data: params
  });
}
