import request from '@/utils/request';
import type {ArticleType} from '@/pages/content/Article/model';
// 定义传参数据类型
type TFindArticleParams = {
  start: number; // 分页开始页
  limit: number; // 分页每页限制
  params?: {   // 条件非必须
    title?: string;  
    author?: string;  
  }
}

/**
 * 查询文章列表 
 * 分页查询
 * @returns 
 */
export async function findArticleList({start,limit, params}: TFindArticleParams): Promise<any> {
  return request(`/lejuAdmin/productArticle/findArticles/${start}/${limit}`,{
    method: 'POST',
    data: params,
  });
}
/**
 * 新增 文章没有id
 * @param params 
 * @returns 
 */
export async function addArticle(params: ArticleType ): Promise<any> {
  return request(`/lejuAdmin/productArticle/addArticle`,{
    method: 'POST',
    data: params,
  });
}
/**
 * 更新 文章有id
 * @param params 
 * @returns 
 */
 export async function updateArticle(params: ArticleType ): Promise<any> {
  return request(`/lejuAdmin/productArticle/updateArticle`,{
    method: 'POST',
    data: params,
  });
}
/**
 * 删除文章
 * @param id 
 * @returns 
 */
export async function deleteArticle(id: string ): Promise<any> {
  return request(`/lejuAdmin/productArticle/del/${id}`,{
    method: 'DELETE'
  })
}
/**
 * 根据id查询文章明细
 * @param id 
 * @returns 
 */
export async function productArticle(id: string ): Promise<any> {
  return request(`/lejuAdmin/productArticle/productArticle/${id}`)
}
