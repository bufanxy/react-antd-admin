interface IUser {
  name: string;
  age: number;
}
// type很多情况下和interface是一样的
type TUser = {
  name: string;
  age: number;
}

const user1: IUser = {name: 'zs',age: 30};
const user2: TUser = {name: 'ls',age: 30};
console.log(user1,user2);
// # 共同点:  都可以描述对象和函数
// interface User {
//   name: string;
//   age: number;
// }
// interface setAge{
//   (age: number): number;
// }

// type User1 = {
//   name: string;
//   age: number;
// }
// type setAge1 = (age: number) => number;
// #区别1:
// ## interface extends interface
interface Name {
  name: string;
}
interface User extends Name {
  age: number;
}
const zs: User = {
  name: '张三',
  age: 30
}
console.log(zs);;
// ## interface extends type
type Name2 = {
  name: string;
}
interface User2 extends Name2 {
  age: number;
}
const zs2: User2 = {
  name: '张三',
  age: 30
}
console.log(zs2);
// ## type 与 type 联合
type Name3 = {
  name: string;
}
type User3 = Name3 & { age: number  };
const zs3: User3 = {
  name: '张三',
  age: 30
}
console.log(zs3);
// ## type 与 interface 交叉
interface Name4 {
  name: string;
}
type User4 = Name4 & {
  age: number;
}
const zs4: User4 = {
  name: '张三',
  age: 30
}
console.log(zs4);
// #区别2:
// type可以声明基本类型别名,interface不行
type Name = string;   // 虽然没有实质性作用
// 联合类型
type Foo = number | string;
const a: Foo = 1;
// typeof赋值
interface Person {
  name: string;
  age: number;
}
