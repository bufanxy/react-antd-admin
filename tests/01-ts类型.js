// 声明类型 类型 boolean  string number  必须用小写
var isDone = false;
var age = 30;
// 在window对象类name属性是window对象的属性,除此之外还有top等
var uname = '张三';
// 数组类型1
var arr = [1, 2, 3];
// 数组类型2 泛型
// const arr2: Array<number> = [1,3,5];
// typescript? anyscript?
// any是任意类型 如果某个阶段不清楚某个变量是什么类型,但又必须声明类型,可以写any
var arr2 = [1, 'a', false];
// Object是所有对象的顶级对象,能否用Object代替any呢? 不行
// Object不能代替any,及时变量上真的存在某个属性,也会报错!
var myName = 'Scott';
console.log(isDone, age, uname);
console.log(arr, arr2);
console.log(myName.length);
// 对函数可以声明返回值类型 void声明没有返回值
function warnUser() {
    console.log("This is my warning message");
}
// 声明一个void类型的变量没有什么大用，因为你只能为它赋予undefined和null：
// const unusable: void = undefined;
console.log(warnUser());
// 类型断言 开始不确定,但在使用的时候明确知道类型
// 平时开发如果知道类型,应该明确标识类型,尽量少使用any 
var myName2 = '张三';
var myNameLength = myName2.length;
// 当你在TypeScript里使用JSX时，只有as语法断言是被允许的。
var myNameLength2 = myName2.length;
console.log(myName2.length, myNameLength, myNameLength2);
