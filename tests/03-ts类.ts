class Greeter {
  // public 成员变量默认的
  public greeting: string;
  constructor(message: string) {
      this.greeting = message;
  }
  // 实际上是定义到了原型对象上
  greet() {
      return `Hello, ${  this.greeting}`;
  }
  // 如果定义为箭头函数 定义为构造函数的成员变量
  say = ()=>{
    console.log('say...');
  }
}

const greeter = new Greeter("world");
greeter.say();
// 因为greeting是public类型的,所以可以通过实例对象访问
console.log(greeter.greeting);
// 关于private 关键词的声明 只能被当前构造函数内部使用,不能被当前外部访问
// protected protected修饰符与private修饰符的行为很相似，但有一点不同，protected成员在派生类中仍然可以访问。
class Animal {
  private name: string;
  // protected name: string;
  constructor(theName: string) { this.name = theName; }
}

class Rhino extends Animal {
  constructor() { super("Rhino"); }
  say(){
    // 这里无法访问 父类中的 private  name属性
    // 但是可以访问protected声明的变量
      console.log('this.name:',this.name);
  }
}
new Animal("Cat").name; // 错误: 'name' 是私有的.

const animal = new Animal("Goat");
const rhino = new Rhino();
// 静态属性  static 声明的变量是属于类(class)的,不需要new就可以访问.
// 比如 Math.PI 
class MyMath {
  static PI = 3.1415926
}

console.log(MyMath.PI);

// 把类当做接口使用 知道可以即可

