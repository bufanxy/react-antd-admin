var Greeter = /** @class */ (function () {
    function Greeter(message) {
        // 如果定义为箭头函数
        this.say = function () {
            console.log('say...');
        };
        this.greeting = message;
    }
    // 原型对象提供了所有实例对象共享的空间,一般把方法放到原型对象上
    Greeter.prototype.greet = function () {
        return "Hello, " + this.greeting;
    };
    return Greeter;
}());
var greeter = new Greeter("world");
greeter.say();
